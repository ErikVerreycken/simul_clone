#ifndef VEHICLE_H
#define VEHICLE_H

#include <vector>
#include <iostream>
#include <boost/property_tree/ptree.hpp>

//#include "Timing.h"
#include "Module.h"
#define PI 3.14

#define INSIZE 0
#define OUTSIZE 3
#define CONFSIZE 0

/*
#define  ALFA1 0
#define  ALFA2 1
#define  FX1   2
#define  FX2   3
#define  FY1   4
#define  FY2   5
#define  MZ1   6
#define  MZ2   7
*/
#define  DU    0
#define  DV    1
#define  DR    2

using boost::property_tree::ptree;
using namespace std;

class ModVehicle: public Module {
 public:
  //ModVehicle(double, double, double, double, double, double, double);
  ModVehicle(int);
  int state_len; // Amount of variables to be updated in RHS
  double y0[11];
  void rhs(const vector<double>*, vector<double>*, const double);
  void write(const vector<double>*, const double);
  void setconfig(ptree);
  void setOutput(int, int);
  void setInput(int, int);
  void writeFile();
  void writeVect(vector<double>);
  vector<double> getY0();
 private:
  double a, b, l, m, Iz, delta, Fy, N;
  double CFa1, CFa2, _t1, _t2;
  vector<double> vect_a;
  vector<double> vect_b;
  vector<double> vect_c;
  vector<double> vect_d;

  double ALFA1;
  double ALFA2;
  double FX1;
  double FX2;
  double FY1;
  double FY2;
  double MZ1;
  double MZ2;
  //double DU;
  //double DV;
  //double DR;
};

#endif
