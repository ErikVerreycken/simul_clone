#ifndef JSON_RW_H
#define JSON_RW_H

#include "../config.h"

#include <iostream>

#include <vector>
#include <sstream>
#include <boost/property_tree/ptree.hpp>

#if defined TYPE_JSON
#include <boost/property_tree/json_parser.hpp>
using boost::property_tree::read_json;
using boost::property_tree::write_json;
#elif  defined TYPE_XML
#include <boost/property_tree/xml_parser.hpp>
using boost::property_tree::read_xml;
using boost::property_tree::write_xml;
#endif

#include <boost/foreach.hpp>

#include "module/Module.h"
#include "ModuleFactory.h"


#define INDENT true


using boost::property_tree::ptree;

using namespace std;

void Json_write(vector<Module*>);
void Json_parse(string, vector<Module*>*, vector<Module*>*);
void Json_print_tree(ptree const&);

#endif
