#include "ModSum.h"

/*ModSum::ModSum(vector<int*> in, vector<int*> out, Timing* t): Module(t), out(out), in(in){
  
  write();
  }*/

ModSum::ModSum(int ID): Module(ID, "Sum", "add two values", 2, 1, 0){}


void ModSum::rhs(const vector<double>* x, vector<double>* dxdt, const double t){
  vector<double> test = in;
  //cout << d(x->at(sum_id), x->at(in1_id) + x->at(in2_id));
  dxdt->at(sum_id) = d(x->at(sum_id), x->at(in1_id) + x->at(in2_id));
  /*cout << "print vals" << test.size() << "\n";
  for (vector<double>::iterator tmp = test.begin(); tmp != test.end(); ++tmp){
    cout << "v: " << *tmp << "\n";
    }*/


  //cout << in1_id << ": " << in.at(in1_id) << ", " << in2_id << ": " << in.at(in2_id) << ", sum: " << in.at(in1_id) + in.at(in2_id) << "here\n";
  //dxdt.at(sum_id) = in.at(in1_id) + in.at(in2_id);
}

void ModSum::write(const vector<double>* x, double t){
  //*(out)[0] = sumValue;
}

void ModSum::setconfig(ptree){
  // No configuration options
}

void ModSum::setOutput(int out_id , int vec_id){
  switch (out_id){
  case 2:
    sum_id = vec_id;
    break;
    //default:
    // raise exception
  }
}

void ModSum::setInput(int in_id, int vec_id){
  //cout << "sum id: " << in_id << ", vec_id: " << vec_id;
  switch (in_id){
  case 0:
    //cout << "a\n";
    in1_id = vec_id;
    //cout << "foo" << in1_id << "bar";
    break;
  case 1:
    //cout << "b\n";    
    in2_id = vec_id;
    //cout << "foo" << in2_id << "bar";
    break;
    //default:
    // raise exception
  }
  //cout << "in1: " << in1_id << ", in2: " << in2_id << "\n";
}

vector<double> ModSum::getY0(){
  vector<double> y0(1, 0);
  return y0;
}
