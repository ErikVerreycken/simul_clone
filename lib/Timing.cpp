#include "Timing.h"

Timing::Timing(int start, int stop, int dt): startTime(start), stopTime(stop),  delta(dt), currentTime(start){}

int Timing::getStart(){
  return startTime;
}

int Timing::getStop(){
  return stopTime;
}

int Timing::getDelta(){
  return delta;
}

int Timing::getCurrentTime(){
  return currentTime;
}

int Timing::tick(){
  currentTime += delta;
  return getCurrentTime();
}
