#include "Json_rw.h"



void Json_write(vector <Module*> modList){

  ptree pt;
  std::ostringstream buf;
  // Version
  pt.put ("version", to_string(VER_V) + "."  + to_string(VER_X) + "."  + to_string(VER_Y) + "."  + to_string(VER_Z));
  int i = 1;
  // Put information for all modules in the JSON file
  for (vector<Module*>::iterator module = modList.begin() ; module != modList.end(); ++module){
    pt.put(to_string(i) + ":.Name", (*module)->getName());
    pt.put(to_string(i) + ":.Description", (*module)->getDescription());
    pt.put(to_string(i) + ":.# Inputs", to_string((*module)->getInSize()));
    pt.put(to_string(i) + ":.# Outputs", to_string((*module)->getOutSize()));
    pt.put(to_string(i) + ":.# Configuration parameters", to_string((*module)->getConfSize()));
    i++;
  }
  
  // Make JSON
  write_json (buf, pt, INDENT);


  // Write JSON
  cout << buf.str();
}

void Json_parse(string input, vector<Module*>* modList, vector<Module*>* blockList){
  ptree pt, blocks, conf, links;
  stringstream ss(input);

  read_json(ss, pt);

  
  // Read all blocks and links into child trees
  blocks = pt.get_child("blocks");
  links = pt.get_child("links");
  //cout << "# of links: " << links.size() << "\n";
   
  // Placeholder for the total amount of outputs
  int numOutputs = 0;

  // Get info from each block
  for(int i = 0; i< blocks.size(); i++){
    // Module ID
    int module_id = blocks.get<int>(to_string(i) + ".module_id");

    // Configuration options
    conf = blocks.get_child(to_string(i) + ".config");
    
    // Create block
    blockList->push_back(createBlock(module_id, conf));

    // Increment amount of outputs
    numOutputs += ((*modList)[module_id])->getOutSize();
    //int size = conf.size();
    //cout << module_id << "\n  " << ((*modList)[module_id])->getOutSize() << size << "\n" ;

  }
  //cout << "numout" << numOutputs << "\n";
  vector<double> outputs(numOutputs, 0);

  // Configure links
  
  //here be errors, runtime, index > link
  for(int i = 0; i<numOutputs; i++){
    int from_block_id = links.get<int>(to_string(i) + ".from_block_id");
    int from_output_id = links.get<int>(to_string(i) + ".from_output_id");
    int to_block_id = links.get<int>(to_string(i) + ".to_block_id");
    int to_input_id = links.get<int>(to_string(i) + ".to_input_id");
    //cout << from_block_id << from_output_id << to_block_id << to_input_id << "\n";
    (*blockList)[from_block_id]->setOutput(from_output_id, i);
    (*blockList)[to_block_id]->setInput(to_input_id, i);
  }
  
  //TODO assert num outputs = num links
  
  //Json_print_tree(pt);
  
}


void Json_print_tree(ptree const& pt){
  // Amount of direct chile elements
  int size = pt.size();
  //cout << "s: " << size << "\n";
  ptree::const_iterator end = pt.end();
  for (ptree::const_iterator it = pt.begin(); it != end; ++it){
    std::cout << it->first << ": " << it->second.get_value<std::string>() << "(" << size << ")\n";
    Json_print_tree(it->second);
  }
}
