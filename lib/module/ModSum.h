#ifndef MODSUM_H
#define MODSUM_H

#include "Module.h"
//#include "Timing.h"
#include <vector>
#include <iostream>
#include <boost/property_tree/ptree.hpp>

using boost::property_tree::ptree;
using namespace std;

class ModSum : public Module{
 public:
  //ModSum(vector<int*> in, vector<int*> out, Timing* t);
  ModSum(int);
  void rhs(const vector<double>*, vector<double>*, const double);
  void write(const vector<double>*, const double);
  void setconfig(ptree);
  void setOutput(int, int);
  void setInput(int, int);
  vector<double> getY0();
 private:
  //vector<int*> in;
  // vector<int*> out;
  int in1_id;
  int in2_id;
  int sum_id;
};

#endif
