#include <iostream>
#include <vector>

//#define PI 3.14

// Config file
#include "config.h"

//#include "lib/Vehicle.h"
#include "lib/Solver.h"
#include "lib/module/Module.h"

// Module factory
#include "lib/ModuleFactory.h"

// JSON interface
#include "lib/Json_rw.h"

int main(int argc, char *argv[]){
  // Modlist see use case 1.2
  vector<Module*> modList = generateModList();

  // BlockList see use case 4
  vector<Module*> blockList;

  // No arguments -> request ModList
  if (argc == 1){
    Json_write(modList);
    return 0;
  }

  // Only accept exactly 5 arguments
  // Argument 2 should be the input string
  if (argc != 5){
    return -1;
  }
  string jsonIn = "{\"version\":\"0.0.0.1\",\"blocks\":{\"0\":{\"module_id\":0,\"config\":\"\"},\"1\":{\"module_id\":\"3\",\"config\":\"\"},\"2\":{\"module_id\":\"3\",\"config\":\"\"},\"3\":{\"module_id\":\"3\",\"config\":\"\"}},\"links\":{\"0\":{\"from_block_id\":\"0\",\"from_output_id\":\"0\",\"to_block_id\":\"1\",\"to_input_id\":\"0\"},\"1\":{\"from_block_id\":\"0\",\"from_output_id\":\"1\",\"to_block_id\":\"2\",\"to_input_id\":\"0\"},\"2\":{\"from_block_id\":\"0\",\"from_output_id\":\"2\",\"to_block_id\":\"3\",\"to_input_id\":\"0\"}}}";
  //string jsonIn = "{\"version\":\"0.0.0.1\",\"blocks\":{\"0\":{\"module_id\":1,\"config\":{\"value\":\"2\"}},\"1\":{\"module_id\":\"1\",\"config\":{\"value\":\"76\"}},\"2\":{\"module_id\":\"2\",\"config\":\"\"},\"3\":{\"module_id\":\"3\",\"config\":\"\"}},\"links\":{\"0\":{\"from_block_id\":\"0\",\"from_output_id\":\"0\",\"to_block_id\":\"2\",\"to_input_id\":\"0\"},\"1\":{\"from_block_id\":\"1\",\"from_output_id\":\"0\",\"to_block_id\":\"2\",\"to_input_id\":\"1\"},\"2\":{\"from_block_id\":\"2\",\"from_output_id\":\"2\",\"to_block_id\":\"3\",\"to_input_id\":\"0\"}}}";
  
  // Parse (and validate) runstring
  Json_parse(jsonIn, &modList, &blockList);

  // Create solver
  //TODO fix this numoutputs (json parse can return it)
  Solver s(blockList, 3);

  // Solve model
  s.solve(0, 1, 0.2);


  /*  double a = 1.3;
  double b = 1.3;
  double m = 600;
  double Iz = 17;
  double delta = 5 * 3.1415 / 180;
  double Fy = 0*500;
  double N = 0*0.3*500;
  */
  cout << "\n% Program terminated succesfully";
  return 0;
}
