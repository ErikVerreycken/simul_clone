#ifndef MODCONSTANT_H
#define MODCONSTANT_H

#include "Module.h"
//#include "Timing.h"
#include <vector>
#include <iostream>
#include <boost/property_tree/ptree.hpp>

//#include "../Json_rw.h"

using boost::property_tree::ptree;
using namespace std;

class ModConstant : public Module{
 public:
  //ModConstant(vector<int*> in, vector<int*> out, vector<int> config, Timing* t);
  ModConstant(int);
  void rhs(const vector<double>*, vector<double>*, const double);
  void write(const vector<double>*, const double);
  void setconfig(ptree);
  void setOutput(int, int);
  void setInput(int, int);
  vector<double> getY0();
 private:
  //In is not needed for a constant
  //vector<double*> out;
  int valueId;
  double constantValue;
};

#endif
