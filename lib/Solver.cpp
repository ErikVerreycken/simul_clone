#include "Solver.h"

// Right hand side
// x: inputs
// y: outputs

//double (Vehicle::*rhs_ptr)(vector<double>, vector<double>), 
	       //void   (Vehicle::*obs_ptr)(vector<double>, double),

Solver::Solver(vector<Module*> v, int numOutputs): blockList(v), numOutputs(numOutputs){
  //Make y0
  FOREACH_BLOCK{
    vector<double> y0ToAdd = (*module)->getY0();
    y0.insert(y0.end(), y0ToAdd.begin(), y0ToAdd.end());
  }

  //Construct y0 by running RHS for t=0
  // here be errors, runtime, segmentation fault
  (*this)(y0, y0, 0);

  initBlocks();
}


// RHS solver
void Solver::operator()(const state_type &x , state_type &dxdt , const double t ){
  for (vector<Module*>::iterator module = blockList.begin(); module != blockList.end(); ++module){
    (*module)->rhs(&x, &dxdt, t);
  }

  //y.at(2) = x.at(1) + x.at(0);
}
 
// Write output
void Solver::operator()( const state_type &x , const double t ){
  //cout << "t: " << t << ", value = " << x.at(2) << "\n";
  for (vector<Module*>::iterator module = blockList.begin(); module != blockList.end(); ++module){
    (*module)->write(&x, t);
  }
}

void Solver::solve(double Tstart, double Tstop, double dt){
  typedef runge_kutta_dopri5< state_type > dopri5_type;
  typedef controlled_runge_kutta< dopri5_type > controlled_dopri5_type;
  typedef dense_output_runge_kutta< controlled_dopri5_type > dense_output_dopri5_type;

  dense_output_dopri5_type dopri5 = make_dense_output( 1E-1 , 1E-1 , dopri5_type() );
  integrate_const( dopri5 , *this , y0 , Tstart , Tstop , dt, *this);

  finalizeBlocks();
}

void Solver::initBlocks(){
  FOREACH_BLOCK{
    (*module)->init();
  }
}
void Solver::finalizeBlocks(){
  FOREACH_BLOCK{
    (*module)->finalize();
  }
}
