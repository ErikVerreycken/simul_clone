#include "ModConstant.h"
/*ModConstant::ModConstant(vector<int*> in, vector<int*> out, vector<int> config, Timing* t): Module(t), out(out), constantValue(config[0]){
  }*/

ModConstant::ModConstant(int ID): Module(ID, "Constant", "a constant value", 0, 1, 1){};


void ModConstant::rhs(const vector<double>* x, vector<double>* dxdt, const double){
  //cout << "size: " << out->size() << ", valID: " << valueId << "\n";
  //dxdt.at(valueId) = constantValue;
}

void ModConstant::write(const vector<double>* x, const double t){
  //Actually nothing needs to be done here
  //*(out)[0] = constantValue; 
}

void ModConstant::setconfig(ptree config){
  //Set conf options
  //cout << config.get<double>("value");
  constantValue = config.get<double>("value");
  //cout << "const: " << constantValue << "\n";
  //Json_print_tree(config);
}

void ModConstant::setOutput(int out_id, int vect_id){
  switch (out_id){
  case 0:
    valueId = vect_id;
    break;
    //default:
    // raise exception
  }
}

void ModConstant::setInput(int id, int vect_id){
  // This function should not be called
  // An exception could be raised here
  //cout << "in_const: " << constantValue << "\n";
}

vector<double> ModConstant::getY0(){
  vector<double> y0(1, constantValue);
  return y0;
}
