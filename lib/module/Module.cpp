#include "Module.h"
Module::Module(int ID, string name, string description, int inSize, int outSize, int confSize): ID(ID), name(name), description(description), inSize(inSize), outSize(outSize), confSize(confSize){

}

/*string Module::getJson(){
  string json = "\t\"" + to_string(ID) + "\": {\n";
  json += "\t\t\"name\": \"" + name + "\",\n";
  json += "\t\t\"description\": \"" + description + "\",\n";
  json += "\t\t\"inputs\": \"" + to_string(inSize) + "\",\n";
  json += "\t\t\"outputs\": \"" + to_string(outSize) + "\",\n";
  json += "\t\t\"config\": \"" + to_string(confSize) + "\"\n";

  json += "\t}\n";

  
  return json;
}
*/

string Module::getName(){
  return name;
}
string Module::getDescription(){
  return description;
}
int Module::getInSize(){
  return inSize;
}
int Module::getOutSize(){
  return outSize;
}
int Module::getConfSize(){
  return confSize;
}

void Module::init(){}
void Module::finalize(){}

// Differentiate
double Module::d(double oldVal, double newVal){
  return newVal-oldVal;
}
