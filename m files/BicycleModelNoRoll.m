function dy = BicycleModelNoRoll(t,y)
global a b m Iz CFa1 CFa2 t1 t2 delta Fy N

% This model is developed to simulate a vehicle undergoing a planar motion.
% The vehicle has only one front and rear tire, the front tire is steerable
% with the steered angle delta. The distance from the center of the front
% tire to the Center of Gravity (CoG) is given by the parameter a. The
% distance from the center of the rear tire with respect to the CoG is 
% called b. The pneumatic trail is given by ti, i = 1,2. The indices 1 and 
% 2 are denoting front and rear respectively.
% The output u, v and r are calculate in the system of coordinates, fixed
% on the vehicle:

% y(1) = u      longitudinal velocity in vehicle system of coordinates
% y(2) = v      lateral velocity in vehicle system of coordinates
% y(3) = r      yaw velocity in vehicle system of coordinates

% All tire forces and moments are assumed to be linear with respect to the 
% slip angle.

% a             distance of the center of the front tire w.r.t. the CoG [m]
% b             distance of the center of the rear tire w.r.t. the CoG [m]
% m             mass of the car [kg]
% Iz            Moment of inertia [kgm�]
% Fxi           longitudinal force acting on tire i [N]
% Fyi           lateral force acting on tire i [N]
% Mzi           alinging torque acting on tire i [Nm]
% ti            pneumatic trail of tire i [m]
% alfai         slip angle of tire i [rad]
% CFai          lateral slip or cornering stiffness of tire i [N/rad]

%% Tire Slip Angles

delta1  = delta;

alfa1   = delta1-(y(2)+a*y(3))/y(1);
alfa2   = -(y(2)-b*y(3))/y(1);

%% Tire Forces and Moments

Fx1     = 0;
Fy1     = CFa1*alfa1;
Fx2     = 0*30000;
Fy2     = CFa2*alfa2;
Mz1     = -t1*CFa1*alfa1;
Mz2     = -t2*CFa2*alfa2;

%% Equations of Motion

du      = Fx1/m-Fy1/m*delta1+Fx2/m+y(2)*y(3);
dv      = Fx1/m*delta1+Fy1/m+Fy2/m-y(1)*y(3)+Fy/m;
dr      = a*Fx1/Iz*delta1+a*Fy1/Iz+Mz1/Iz-b*Fy2/Iz+Mz2/Iz+N/Iz;

dy      = zeros(3,1);
dy(1)   = du;
dy(2)   = dv;
dy(3)   = dr;