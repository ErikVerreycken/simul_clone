global a b m Iz CFa1 CFa2 t1 t2 delta Fy N

sim     = 0;


%% Input

delta   = 5*pi/180;
Fy      = 0*500;
N       = 0*-0.3*500;
V       = 50;


%% Vehicle Parameters

a       = 1.0;
b       = 1.6;
m       = 600;
Iz      = 17;       % kg*m�
l       = a+b;


%% Tire Parameters

CFa1    = 117343;%*180/pi;
CFa2    = 117343;%*180/pi;
t1      = 0.0;
t2      = 0.0;
o% C1      = -CFa1;
% C2      = -CFa2;

% Yb      = C1+C2;
% Yr      = 1/V*(a*C1-b*C2);
% Yd      = -C1;
% Nb      = a*C1-b*C2;
% Nr      = 1/V*(a^2*C1+b^2*C2);
% Nd      = -a*C1;
% Q       = Nb*Yr-Nb*m*V-Yb*Nr;
% K       = 1/l*Nb*m/(Yd*Nb-Yb*Nd);
%V       = sqrt(-1/K);


%% Simulation

dt      = 0.002;
tend    = 1;
lims    = [-5 5 -5 5];

% ode45 => Runge Kuta solver
options = odeset('RelTol',1e-4,'AbsTol',[1e-4 1e-4 1e-5]);

% GNU scientific library c/c++
[T1,Y1] = ode45(@BicycleModelNoRoll,0:dt:tend,[V 0 0],options);

figure(1);

subplot(3,1,1);
plot(T1,Y1(:,1),'linewidth',2); hold on; grid on;
ylabel('Lon. velocity (m/s)');
xlabel('Time (s)');

subplot(3,1,2);
plot(T1,Y1(:,2),'linewidth',2); hold on; grid on;
ylabel('Lat. velocity (m/s)');
xlabel('Time (s)');

subplot(3,1,3);
plot(T1,Y1(:,3)*180/pi,'linewidth',2); hold on; grid on;
ylabel('Angular velocity (�/s)');
xlabel('Time (s)');

figure(2);

delta1      = delta;%*sin(2*pi*3*T1);
% delta1      = delta;
plot(T1,(delta1-(Y1(:,2)+a*Y1(:,3))./Y1(:,1))*180/pi,'g','linewidth',2); hold on; grid on;
plot(T1,(-(Y1(:,2)-b*Y1(:,3))./Y1(:,1))*180/pi,'r','linewidth',2); hold on; grid on;
legend('Front SA','Rear SA');
ylabel('Slip Angles (�)');
xlabel('Time (s)');
