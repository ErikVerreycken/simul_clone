#include "ModuleFactory.h"

vector<Module*> generateModList(){
  int ID = 0;
  vector<Module*> modList;

  // Vehicle - 0
  Module* v = new ModVehicle(ID++);
  modList.push_back(v);
  
  // Constant - 1
  Module* c = new ModConstant(ID++);
  modList.push_back(c);

  // Sum - 2
  Module* s = new ModSum(ID++);
  modList.push_back(s);

  // Output - 3
  Module* o = new ModOutput(ID++);
  modList.push_back(o);
  
  return modList;
}

Module* createBlock(int module_id, ptree config){
  Module* block;  
  switch(module_id){
  case 0:
    block = new ModVehicle(0);
    block->setconfig(config);
    break;
  case 1:
    block = new ModConstant(0);
    block->setconfig(config);
    break;
  case 2:
    block = new ModSum(0);
    block->setconfig(config);
    break;
  case 3:
    block = new ModOutput(0);
    block->setconfig(config);
    break;
    //default:
    //Throw error
  }
  return block;
}
