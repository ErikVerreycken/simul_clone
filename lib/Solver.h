// Solver takes y0 as an array of initial values  

#ifndef SOLVER_H
#define SOLVER_H

//
#define FOREACH_BLOCK for (vector<Module*>::iterator module = blockList.begin(); module != blockList.end(); ++module)

//#include <list>
#include <vector>

#include <boost/array.hpp>
#include <boost/numeric/odeint/config.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/stepper/bulirsch_stoer.hpp>
#include <boost/numeric/odeint/stepper/bulirsch_stoer_dense_out.hpp>

//#include "module/Vehicle.h"
#include "module/Module.h"

using namespace std;
using namespace boost::numeric::odeint;

//typedef boost::array<double, 3> state_type;
typedef vector<double> state_type;

//	 double (Vehicle::*)(vector<double>, vector<double>), 
//       void   (Vehicle::*)(vector<double>, double), Vehicle*);


class Solver {
 public:
  //Solver(double [], vector<Module*>);
  Solver(vector<Module*>, int);
  void operator()(const state_type&, state_type&, const double);
  void operator()(const state_type&, const double);
  void solve(double, double, double);
  state_type y0;
  //double (Vehicle::*rhs_ptr)(vector<double>, vector<double>);
  //void   (Vehicle::*obs_ptr)(vector<double>, const double);
  //Vehicle* v;
  int numOutputs;
  vector<Module*> blockList;
 private:
  void initBlocks();
  void finalizeBlocks();
};

#endif
