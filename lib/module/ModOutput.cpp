#include "ModOutput.h"

/*ModOutput::ModOutput(vector<int*> in, vector<int*> out, vector<int> config, Timing*t): Module(t), in(in){}

void ModOutput::tick(){}

void ModOutput::write(){
  cout << "at t=" << t->getCurrentTime() << "\tvalue=" << *(in[0]) << "\n";
}
*/

ModOutput::ModOutput(int ID): Module(ID, "Output", "write output to a matlab plot", 1, 0, 0){
  //cout << "v = [];\n";
  //cout << "t = [];\n";
  out << "v" << ID << " = [];\n";
  out << "t = [];\n";
};

void ModOutput::rhs(const vector<double>* x, vector<double>* dxdt, const double){}

void ModOutput::write(const vector<double>* x, double t){
  //cout << "at t: " << t << ", value: " << x->at(valueId) << "\n";
  out << "v = [v " << x->at(valueId) << "];\n";
  out << "t = [t " << t << "];\n";
}

void ModOutput::setconfig(ptree){
  // No current configuration options
}

void ModOutput::setOutput(int out_id, int vect_id){
  //raise exception
}

void ModOutput::setInput(int in_id, int vect_id){
  switch (in_id){
  case 0:
    valueId = vect_id;
    break;
    //default:
    // raise exception
  }
}

vector<double> ModOutput::getY0(){
  return vector<double>(0, 0);
}

void ModOutput::finalize(){
  cout << out.str() << "\n plot(t,v);" ;
}
