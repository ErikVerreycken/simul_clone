#ifndef MODULEFACTORY_H
#define MODULEFACTORY_H

#include <vector>

#include "module/Module.h"
#include "module/ModVehicle.h"
#include "module/ModConstant.h"
#include "module/ModSum.h"
#include "module/ModOutput.h"

using namespace std;

vector<Module*> generateModList();
Module* createBlock (int, ptree);

#endif
