#ifndef MODULE
#define MODULE

#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>

using boost::property_tree::ptree;

using namespace std;

class Module {
 public:
  Module(int, string, string, int, int, int);
  // The right hand side function
  // This function gets solved
  virtual void rhs(const vector<double>*, vector<double>*, const double)=0;
  
  // The output/observer function
  virtual void write(const vector<double>*, const double)=0;

  // Set configuration options (from Json file)
  virtual void setconfig(ptree)=0;
  virtual void setOutput(int, int)=0;
  virtual void setInput(int, int)=0;
  virtual vector<double> getY0() = 0;
  virtual void init();
  virtual void finalize();
  //protected:
  //Timing* t;
  //string getJson();
  string getName();
  string getDescription();
  int getInSize();
  int getOutSize();
  int getConfSize();
 protected:
  int ID;
  string name;
  string description;
  int inSize;
  vector<double> in;
  int outSize;
  vector<double> out;
  int confSize;
  vector<double> conf;
  double d(double, double);
};


#endif
