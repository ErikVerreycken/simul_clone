#include "ModVehicle.h"

ModVehicle::ModVehicle(int ID): Module(ID, "Vehicle", "a simple vehicle", INSIZE, OUTSIZE, CONFSIZE){
  delta = 5 * 3.1415 / 180;
  Fy = 0*500;
  N = 0*-0.3*500;
  a = 1.3;
  b = 1.3;
  l = a + b;
  m = 600;
  Iz = 17;
  CFa1 = 117343;
  CFa2 = 117343;
  _t1 = 0.0;
  _t2 = 0.0;
};

/*ModVehicle::ModVehicle(double a, double b, double m, double Iz, double delta, double Fy, double N):
  a(a),
  b(b),
  l(a+b),
  m(m),
  Iz(Iz),
  delta(delta),
  Fy(Fy),
  N(N),
  y0 {0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0}

{
  CFa1 = 117343;
  CFa2 = 117343;
  _t1 = 0;
  _t2 = 0;
  state_len = 11;
  ALFA1 = 0;
  ALFA2 = 0;
  FX1 = 0;
  FX2 = 0;
  FY1 = 0;
  FY2 = 0;
  MZ1 = 0;
  MZ2 = 0;
  //DU = 0;
  //DV = 0;
  //DR = 0;
  }*/

void ModVehicle::rhs(const vector<double>* x, vector<double>* dxdt, const double t){
  //cout << "t: " << t << "\n";
  ALFA1 = (delta - (x->at(DR) + a*x->at(DV))/x->at(DU));
  ALFA2 = (-1* ( x->at(DR) - b*x->at(DV))/x->at(DU));   // Tyre property slip angle [rad]
  FX1 = 0;
  FX2 = 0;     // Tyre property longituninal force [N]
  FY1 = CFa1 * ALFA1;
  FY2 = CFa2 * ALFA2;     // Tyre property lateral force [N]
  MZ1 = (-1 * _t1 * CFa1 * ALFA1);
  MZ2 = (-1 * _t2 * CFa2 * ALFA2);     // Tyre property aligning torque [N*m] 

  //cout <<"% DU: " << x->at(DU) << "\n\n";

  /*  cout <<"% ALFA1: " << ALFA1 << "\n";
  cout <<"% delta: " << delta << "\n";
  cout <<"% x->at(DR): " << x->at(DR) << "\n";
  cout <<"% a: " << a << "\n";
  cout <<"% x->at(DV): " << x->at(DV) << "\n";
  cout <<"% DU: " << x->at(DU) << "\n";

  cout <<"% FX1: " << FX1 << "\n";
  cout <<"% m: " << m << "\n";
  cout <<"% FY1: " << FY1 << "\n";
  cout <<"% FX2: " << FX2 << "\n";
  cout <<"% cfa1: " << CFa1 << "\n";

  cout <<"% uuuU: " << (FX1)/m - (FY1)/m*delta + (FX2)/m + x->at(DR)*x->at(DV) << "\n";*/

  dxdt->at(DU) += (FX1)/m - (FY1)/m*delta + (FX2)/m + x->at(DR)*x->at(DV);
  dxdt->at(DR) += (FX1)/m*delta + (FY1)/m + (FY2)/m - x->at(DU)*x->at(DV) + Fy/m;
  dxdt->at(DV) += a*(FX1)/Iz*delta + a*(FY1)/Iz + (MZ1)/Iz - b*(FY2)/Iz + (MZ2)/Iz + N/Iz;
}

void ModVehicle::write(const vector<double>* x, double t){
  /*  vect_a->push_back(values[DU]);
  vect_b->push_back(values[DR]);
  vect_c->push_back(values[DV]);
  vect_d->push_back(t);*/
}

void ModVehicle::setconfig(ptree){
  // TODO add configuration options
}

void ModVehicle::writeVect(vector<double> myVect){
  cout << "[";
  for (vector<double>::iterator it = myVect.begin(); it != myVect.end(); ++it){
    cout << *it << ",";
  }
  cout << "];\n";
}



void ModVehicle::writeFile(){
  cout << "tic();\n system('./main.o > visualize.m');\n toc();\n";
  
  cout << "d = ";  
  writeVect(vect_d);
  
  cout << "a = ";
  writeVect(vect_a);

  cout << "b = ";
  writeVect(vect_b);

  cout << "c = ";
  writeVect(vect_c);

  cout << "subplot(3,1,1);\n plot(d, a); hold on; grid on; ylabel('du'); xlabel('t'); \n";
  cout << "subplot(3,1,2);\n plot(d, b); hold on; grid on; ylabel('dv'); xlabel('t'); \n";								      cout << "subplot(3,1,3);\n plot(d, c*180/3.1415); hold on; grid on; ylabel('dr'); xlabel('t'); \n";
  cout << "print -djpg fig.jpg";
}

void ModVehicle::setOutput(int out_id, int vect_id){

}

void ModVehicle::setInput(int in_id, int vect_id){

}

vector<double> ModVehicle::getY0(){
  vector<double> tmp;
  tmp.push_back(50);
  tmp.push_back(0);
  tmp.push_back(0);
  return tmp;

  //TODO fix here
  //return vector<double>(OUTSIZE,0);
}
