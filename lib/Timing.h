#ifndef TIMING_H
#define TIMING_H

class Timing {
 public:
  Timing(int, int, int);
  int getStart();
  int getStop();
  int getDelta();
  int getCurrentTime();
  int tick();
 private:
  int startTime;
  int stopTime;
  int delta;
  int currentTime;
};

#endif
